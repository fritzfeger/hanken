module Application.Helper.View where
import qualified Text.MMark as MMark


import IHP.ViewPrelude
import Web.Controller.Prelude


-- Text

hankenTitle :: Text
hankenTitle = "Hanken Entrup"

hankenMissionStatement :: Text
hankenMissionStatement = "Kartoffeln, Erdbeeren und Eier direkt vom Erzeuger"

hankenSlogan :: Text
hankenSlogan = "Hanken Entrup – 100% regional, natürlich, lecker"

hankenWoErhaeltlich :: Text
hankenWoErhaeltlich = "Wo bekomme ich die Produkte?"

hankenWasAktuellProdukt :: Text 
hankenWasAktuellProdukt = "Was gibt es Neues zu berichten?"

hankenLogoUrl :: Text
hankenLogoUrl = "../img/logo/Hanken_Logo.svg"

hankenErdbeereUrl :: Text
hankenErdbeereUrl = "../img/logo/Hanken_Erdbeere.svg"

hankenSchriftzugUrl :: Text
hankenSchriftzugUrl = "../img/logo/Hanken_Schriftzug.svg"


-- Img Responsive
data ImgParams = ImgParams
    { minWidth :: Integer
    , filesize   :: Integer
    }

imgHD     :: ImgParams
imgHD      = ImgParams {minWidth = 1201, filesize = 1920}
imgBig    :: ImgParams
imgBig     = ImgParams {minWidth = 961, filesize = 1110}
imgMedium :: ImgParams
imgMedium  = ImgParams {minWidth = 768, filesize = 690}
imgSmall  :: ImgParams
imgSmall   = ImgParams {minWidth = 1, filesize = 545}

imgMinWidthHD     :: Text
imgMinWidthHD      = "(min-width: " <> show (filesize imgHD) <> "px)"
imgMinWidthBig    :: Text
imgMinWidthBig     = "(min-width: " <> show (filesize imgBig) <> "px)"
imgMinWidthMedium :: Text
imgMinWidthMedium  = "(min-width: " <> show (filesize imgMedium) <> "px)"
imgMinWidthSmall  :: Text
imgMinWidthSmall   = "(min-width: " <> show (filesize imgSmall) <> "px)"

imgPathStaticHD     :: Text -> Text
imgPathStaticHD     imgName = "../img/static/" <> imgName <> "_" <> show (filesize imgHD)     <> ".webp"
imgPathStaticBig    :: Text -> Text
imgPathStaticBig    imgName = "../img/static/" <> imgName <> "_" <> show (filesize imgBig)    <> ".webp"
imgPathStaticMedium :: Text -> Text
imgPathStaticMedium imgName = "../img/static/" <> imgName <> "_" <> show (filesize imgMedium) <> ".webp"
imgPathStaticSmall  :: Text -> Text
imgPathStaticSmall  imgName = "../img/static/" <> imgName <> "_" <> show (filesize imgSmall)  <> ".webp"

-- Enums to Text

placeTypeCustom :: PlaceType -> Text
placeTypeCustom ErdbeerSelbstpflueckfeldMitVerkaufsstand = "Erdbeer-Selbstpflückfeld mit Verkaufsstand"
placeTypeCustom ErdbeerVerkaufsstand                     = "Reiner Erdbeer-Verkaufsstand"
placeTypeCustom ErdbeerSupermarkt                        = "Erdbeerverkauf im Supermarkt"
placeTypeCustom KartoffelEierVerkaufshaus                = "Kartoffel- und Eier-Häuschen mit Selbstbedienung"
placeTypeCustom KartoffelVerkaufshaus                    = "Kartoffel-Häuschen mit Selbstbedienung"
placeTypeCustom KartoffelnSupermarkt                     = "Kartoffelverkauf im Supermarkt"

productCustom :: Product -> Text
productCustom Erdbeeren      = "Erdbeeren"
productCustom Kartoffeln     = "Kartoffeln"
productCustom Kuerbisse      = "Kürbisse"
productCustom Eier           = "Eier"
productCustom Betrieb        = "Betrieb"
productCustom Nachhaltigkeit = "Nachhaltigkeit"

-- HTML

renderImgFullWidth :: Text -> Html
renderImgFullWidth imgName = [hsx|
    <div style="--aspect-ratio:3/1;">
        <picture>
            <source media={imgMinWidthHD} srcset={imgPathStaticHD imgName}/>
            <source media={imgMinWidthBig} srcset={imgPathStaticBig imgName}/>
            <source media={imgMinWidthMedium} srcset={imgPathStaticMedium imgName}/>
            <img src={imgPathStaticSmall imgName} class="image-full-width" alt={imgName}/>
        </picture>
    </div>
|]

renderBrandMenu :: Html
renderBrandMenu = [hsx|
    <img src={hankenErdbeereUrl} alt="Logo Hanken Entrup" width="50" height="71">
    <img src={hankenSchriftzugUrl} alt="Logo Hanken Entrup" width="130" height="36" style="margin-top:22px;">&nbsp;&nbsp;&nbsp;
|]

renderSlogan :: Html
renderSlogan = [hsx|
    <div class="immergruen-full-width">
        {hankenSlogan}
    </div>
|]

saisonStatusBadge :: Saison -> Html
saisonStatusBadge status = case status of
    Laeuft      -> [hsx|<p class="badge badge-pill badge-secondary">läuft</p>|]  -- soll weg, aber im Moment wg. Umlaut geht nicht leider nicht zu ändern...
    Durchgehend -> [hsx|<p class="badge badge-pill badge-secondary">durchgehend</p>|]
    Pausiert    -> [hsx|<p class="badge badge-pill badge-primary">pausiert</p>|]

saisonStatusSortierreihenfolge :: Saison -> Int
saisonStatusSortierreihenfolge status = case status of
    Laeuft      -> 1
    Durchgehend -> 2
    Pausiert    -> 3

renderFooter :: Html
renderFooter = [hsx|
    <footer class="container-fluid">
        <div class="footer-content" >
            <div class="">
                <a href={WelcomeAction}>
                    <img src="../img/logo/Hanken_Logo.svg" alt="Logo" width="180" height="130.5">
                </a>
            </div>
            <div class="">
                <p><strong>Produkte</strong></p>
                <p>
                    <a href={ErdbeerenAction}>Erdbeeren</a><br>
                    <a href={KartoffelnAction}>Kartoffeln</a><br>
                    <a href={KuerbisseAction}>Kürbisse</a><br>
                    <a href={EierAction}>Eier</a><br>
                </p>
            </div>
            <div class="">
                <p><strong>Betrieb</strong></p>
                <p>
                    <a href={PlacesAction}>Verkaufsstellen</a><br>
                    <a href={NachhaltigkeitAction}>Nachhaltigkeit</a><br>
                    <a href={ReiterhofAction }>Reiterhof</a><br>
                    <a href={PostsAction}>Aktuelles</a><br>
                </p>
            </div>
            <div class="">
                <p><strong>Soziale Medien</strong></p>
                <p>
                    <!-- https://github.com/kenangundogan/fontisto -->
                    <a href="https://www.facebook.com/pages/Hanken-Entrup/399878797056339"><!--Generated by Fontisto--><svg viewBox="0 0 12 24" xmlns="http://www.w3.org/2000/svg"><title>Facebook</title><path d="m12.462.173v3.808h-2.265c-.079-.011-.171-.017-.264-.017-.542 0-1.036.203-1.411.538l.002-.002c-.275.384-.439.863-.439 1.381 0 .062.002.124.007.185v-.008 2.726h4.229l-.56 4.27h-3.663v10.946h-4.417v-10.947h-3.68v-4.269h3.68v-3.145c-.007-.102-.011-.222-.011-.342 0-1.478.575-2.822 1.513-3.82l-.003.003c.972-.92 2.288-1.485 3.735-1.485.09 0 .18.002.27.007h-.013c.118-.002.256-.003.395-.003 1.02 0 2.025.064 3.011.188l-.117-.012z"/></svg></a>&nbsp;
                    &nbsp;
                    <a href="https://www.instagram.com/hanken_entrup/"><!--Generated by Fontisto--><svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>Instagram</title><path d="m16 12v-.001c0-2.209-1.791-4-4-4s-4 1.791-4 4 1.791 4 4 4c1.104 0 2.104-.448 2.828-1.171.723-.701 1.172-1.682 1.172-2.768 0-.021 0-.042-.001-.063v.003zm2.16 0c-.012 3.379-2.754 6.114-6.135 6.114-3.388 0-6.135-2.747-6.135-6.135s2.747-6.135 6.135-6.135c1.694 0 3.228.687 4.338 1.797 1.109 1.08 1.798 2.587 1.798 4.256 0 .036 0 .073-.001.109v-.005zm1.687-6.406v.002c0 .795-.645 1.44-1.44 1.44s-1.44-.645-1.44-1.44.645-1.44 1.44-1.44c.398 0 .758.161 1.018.422.256.251.415.601.415.988v.029-.001zm-7.84-3.44-1.195-.008q-1.086-.008-1.649 0t-1.508.047c-.585.02-1.14.078-1.683.17l.073-.01c-.425.07-.802.17-1.163.303l.043-.014c-1.044.425-1.857 1.237-2.272 2.254l-.01.027c-.119.318-.219.695-.284 1.083l-.005.037c-.082.469-.14 1.024-.159 1.589l-.001.021q-.039.946-.047 1.508t0 1.649.008 1.195-.008 1.195 0 1.649.047 1.508c.02.585.078 1.14.17 1.683l-.01-.073c.07.425.17.802.303 1.163l-.014-.043c.425 1.044 1.237 1.857 2.254 2.272l.027.01c.318.119.695.219 1.083.284l.037.005c.469.082 1.024.14 1.588.159l.021.001q.946.039 1.508.047t1.649 0l1.188-.024 1.195.008q1.086.008 1.649 0t1.508-.047c.585-.02 1.14-.078 1.683-.17l-.073.01c.425-.07.802-.17 1.163-.303l-.043.014c1.044-.425 1.857-1.237 2.272-2.254l.01-.027c.119-.318.219-.695.284-1.083l.005-.037c.082-.469.14-1.024.159-1.588l.001-.021q.039-.946.047-1.508t0-1.649-.008-1.195.008-1.195 0-1.649-.047-1.508c-.02-.585-.078-1.14-.17-1.683l.01.073c-.07-.425-.17-.802-.303-1.163l.014.043c-.425-1.044-1.237-1.857-2.254-2.272l-.027-.01c-.318-.119-.695-.219-1.083-.284l-.037-.005c-.469-.082-1.024-.14-1.588-.159l-.021-.001q-.946-.039-1.508-.047t-1.649 0zm11.993 9.846q0 3.578-.08 4.953c.005.101.009.219.009.337 0 3.667-2.973 6.64-6.64 6.64-.119 0-.237-.003-.354-.009l.016.001q-1.375.08-4.953.08t-4.953-.08c-.101.005-.219.009-.337.009-3.667 0-6.64-2.973-6.64-6.64 0-.119.003-.237.009-.354l-.001.016q-.08-1.375-.08-4.953t.08-4.953c-.005-.101-.009-.219-.009-.337 0-3.667 2.973-6.64 6.64-6.64.119 0 .237.003.354.009l-.016-.001q1.375-.08 4.953-.08t4.953.08c.101-.005.219-.009.337-.009 3.667 0 6.64 2.973 6.64 6.64 0 .119-.003.237-.009.354l.001-.016q.08 1.374.08 4.953z"/></svg></a>
                </p>
            </div>
            <div class="">
                <p><strong>Kontakt</strong></p>
                <address>
                    Hanken Obst GbR<br>
                    Entruper Weg 194<br>
                    32657 Lemgo<br>
                    Telefon 05261 4146<br>
                    <a href = "mailto:info@hanken-entrup.de">info@hanken-entrup.de</a>
                </address>
            </div>
            <div class="">
                <p><strong>Legal</strong></p>
                <p>
                    <a href="/impressum">Impressum</a><br>
                    <a href="/datenschutz">Datenschutz</a><br>
                    <a href="/admin/NewSession">Login</a><br><br>
                </p>
            </div>
        </div>
    </footer>
|]
    where
        renderProduct product = [hsx|
            <a href={produktPathQuery product}>{product}</a><br>
        |]
            where
                produktPathQuery product = "/posts/?product=" <> show product |> toLower

renderMarkdown :: Text -> Html
renderMarkdown text = 
    case text |> MMark.parse "" of
        Left error -> "no valid Markdown"
        Right markdown ->
                markdown
                |> MMark.render
                |> tshow
                |> preEscapedToHtml


-- Meta

-- The 'assetPath' function used below appends a `?v=SOME_VERSION` to the static assets in production
-- This is useful to avoid users having old CSS and JS files in their browser cache once a new version is deployed
-- See https://ihp.digitallyinduced.com/Guide/assets.html for more details

stylesheets :: Html
stylesheets = [hsx|
        <link rel="stylesheet" href={assetPath "/vendor/bootstrap.min.css"}/>
        <link rel="stylesheet" href={assetPath "/vendor/flatpickr.min.css"}/>
        <link rel="stylesheet" href={assetPath "/app.css"}/>
        <!-- https://google-webfonts-helper.herokuapp.com/fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    |]

scriptsHead :: Html
scriptsHead = [hsx|
        <script src={assetPath "/vendor/turbolinks.js"}></script>
        <script src={assetPath "/vendor/turbolinksInstantClick.js"}></script>
        <script src={assetPath "/vendor/turbolinksMorphdom.js"}></script>
|]

scriptsBottom :: Html
scriptsBottom = [hsx|
        {when isDevelopment devScripts}
        <script src={assetPath "/vendor/jquery-3.6.0.slim.min.js"}></script>
        <script src={assetPath "/vendor/timeago.js"}></script>
        <script src={assetPath "/vendor/popper.min.js"}></script>
        <script src={assetPath "/vendor/bootstrap.min.js"}></script>
        <script src={assetPath "/vendor/flatpickr.js"}></script>
        <script src={assetPath "/vendor/morphdom-umd.min.js"}></script>
        <script src={assetPath "/helpers.js"}></script>
        <script src={assetPath "/ihp-auto-refresh.js"}></script>
        <script src={assetPath "/app.js"}></script>
    |]

devScripts :: Html
devScripts = [hsx|
        <script id="livereload-script" src={assetPath "/livereload.js"} data-ws={liveReloadWebsocketUrl}></script>
    |]

metaTags :: Html
metaTags = [hsx|
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta property="og:title" content="App"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="TODO"/>
    <meta property="og:description" content="TODO"/>
    {autoRefreshMeta}
|]


-- Instances

instance InputValue Point where
    inputValue point = inputValue (get #x point) <> "," <> inputValue (get #y point)

    