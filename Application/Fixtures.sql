

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;


SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE public.places DISABLE TRIGGER ALL;

INSERT INTO public.places (id, name, img_filename, location, description, hours, product, place_type, created_at, saison) VALUES ('26dfff9e-2843-4d48-aae9-0eaece6a4e5d', 'Haselünne', 'Entrup.webp', '(5.5,2.2)', 'sdffdsfdsfdsfds', '15–19', 'kartoffeln', 'kartoffel_verkaufsstand', '2021-12-27 23:31:46.582452+01', 'pausiert');


ALTER TABLE public.places ENABLE TRIGGER ALL;


ALTER TABLE public.posts DISABLE TRIGGER ALL;

INSERT INTO public.posts (id, title, img_filename, excerpt, product, body, created_at, visit_count) VALUES ('d170c48b-7f74-444a-964a-66b008d6313f', 'sdfdwfd', 'Hanken-Entrup-Schimmel.webp', 'asdfswwdfw', 'erdbeeren', 'asdfsdfssdf', '2021-12-14 01:00:00+01', 11);
INSERT INTO public.posts (id, title, img_filename, excerpt, product, body, created_at, visit_count) VALUES ('086ca3a8-ee1e-45c8-92dc-d99b74481529', 'asdffdsfdsaa', '', 'sdffdsa', 'kartoffeln', 'asdfdsasfd', '2021-12-23 01:00:00+01', 2);
INSERT INTO public.posts (id, title, img_filename, excerpt, product, body, created_at, visit_count) VALUES ('fa4803c4-7133-46c4-8a71-ea5bdaea1a59', 'Längerer Titel wie im echten Leben', 'Hanken-Entrup-Schimmel.webp', 'Das Exzerpt hat so ca. ein bis zwei Sätze. Es sollte in diese Zeile auf jeden Fall reinpassen. Dieses Eingabefeld hat noch Platz... bis... hier!', 'erdbeeren', 'Das Exzerpt hat so ca. ein bis zwei Sätze. Es sollte in diese Zeile auf jeden Fall reinpassen. Dieses Eingabefeld hat noch Platz... bis... hier! Das Exzerpt hat so ca. ein bis zwei Sätze. Es sollte in diese Zeile auf jeden Fall reinpassen. Dieses Eingabefeld hat noch Platz... bis... hier! Das Exzerpt hat so ca. ein bis zwei Sätze. Es sollte in diese Zeile auf jeden Fall reinpassen. Dieses Eingabefeld hat noch Platz... bis... hier! Das Exzerpt hat so ca. ein bis zwei Sätze. Es sollte in diese Zeile auf jeden Fall reinpassen. Dieses Eingabefeld hat noch Platz... bis... hier! Das Exzerpt hat so ca. ein bis zwei Sätze. Es sollte in diese Zeile auf jeden Fall reinpassen. Dieses Eingabefeld hat noch Platz... bis... hier! Das Exzerpt hat so ca. ein bis zwei Sätze. Es sollte in diese Zeile auf jeden Fall reinpassen. Dieses Eingabefeld hat noch Platz... bis... hier! Das Exzerpt hat so ca. ein bis zwei Sätze. Es sollte in diese Zeile auf jeden Fall reinpassen. Dieses Eingabefeld hat noch Platz... bis... hier!

Das Exzerpt hat so ca. ein bis zwei Sätze. Es sollte in diese Zeile auf jeden Fall reinpassen. Dieses Eingabefeld hat noch Platz... bis... hier! Das Exzerpt hat so ca. ein bis zwei Sätze. Es sollte in diese Zeile auf jeden Fall reinpassen. Dieses Eingabefeld hat noch Platz... bis... hier!', '2021-12-16 01:00:00+01', 89);


ALTER TABLE public.posts ENABLE TRIGGER ALL;


ALTER TABLE public.schema_migrations DISABLE TRIGGER ALL;

INSERT INTO public.schema_migrations (revision) VALUES (1641142163);


ALTER TABLE public.schema_migrations ENABLE TRIGGER ALL;


ALTER TABLE public.users DISABLE TRIGGER ALL;

INSERT INTO public.users (id, email, password_hash, failed_login_attempts, logins, locked_at) VALUES ('8b9a4c26-ac90-40ed-9de0-2aa01be274c0', 'ff@fritzfeger.com', 'sha256|17|4Qz87XCSoD0Hox453rN2cg==|fRopeS77KS5ObVoAybi3K5dfQqrIvBkUTpf4ku3hmLg=', 0, 6, NULL);


ALTER TABLE public.users ENABLE TRIGGER ALL;


