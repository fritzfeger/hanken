CREATE TYPE products AS ENUM ('erdbeeren', 'kartoffeln', 'eier', 'kuerbisse', 'betrieb', 'nachhaltigkeit');
CREATE TYPE place_types AS ENUM ('erdbeer_selbstpflueckfeld_mit_verkaufsstand', 'erdbeer_verkaufsstand', 'kartoffel_verkaufshaus', 'kartoffeln_supermarkt', 'kartoffel_eier_verkaufshaus', 'erdbeer_supermarkt');
CREATE TYPE saisons AS ENUM ('laeuft', 'pausiert', 'durchgehend');
CREATE TABLE posts (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY NOT NULL,
    title TEXT NOT NULL,
    img_filename TEXT NOT NULL,
    excerpt TEXT NOT NULL,
    product products NOT NULL,
    body TEXT NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
    visit_count INT DEFAULT 0 NOT NULL
);
CREATE TABLE places (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    img_filename TEXT NOT NULL,
    "location" POINT NOT NULL,
    description TEXT NOT NULL,
    hours TEXT NOT NULL,
    product products NOT NULL,
    place_type place_types NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
    saison saisons NOT NULL
);
CREATE TABLE users (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY NOT NULL,
    email TEXT NOT NULL,
    password_hash TEXT NOT NULL,
    failed_login_attempts INT DEFAULT 0 NOT NULL,
    logins INT DEFAULT 0 NOT NULL,
    locked_at TIMESTAMP WITH TIME ZONE DEFAULT NULL
);
