module Admin.Types where

import IHP.Prelude
import IHP.ModelSupport
import Generated.Types
import IHP.LoginSupport.Types

data AdminApplication = AdminApplication deriving (Eq, Show)


data StaticController = WelcomeAction deriving (Eq, Show, Data)

data PostsController
    = PostsAction
    | NewPostAction
    | ShowPostAction { postId :: !(Id Post) }
    | CreatePostAction
    | EditPostAction { postId :: !(Id Post) }
    | UpdatePostAction { postId :: !(Id Post) }
    | DeletePostAction { postId :: !(Id Post) }
    deriving (Eq, Show, Data)

data PlacesController
    = PlacesAction
    | NewPlaceAction
    | ShowPlaceAction { placeId :: !(Id Place) }
    | CreatePlaceAction
    | EditPlaceAction { placeId :: !(Id Place) }
    | UpdatePlaceAction { placeId :: !(Id Place) }
    | DeletePlaceAction { placeId :: !(Id Place) }
    deriving (Eq, Show, Data)

data SessionsController
    = NewSessionAction
    | CreateSessionAction
    | DeleteSessionAction
    deriving (Eq, Show, Data)

instance HasNewSessionUrl User where
    newSessionUrl _ = "/admin/NewSession"

type instance CurrentUserRecord = User