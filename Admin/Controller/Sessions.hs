module Admin.Controller.Sessions where

import Admin.Controller.Prelude
import Admin.View.Sessions.New
import qualified IHP.AuthSupport.Controller.Sessions as Sessions
import Web.View.Prelude (PostController(PostsAction))

instance Controller SessionsController where
    action NewSessionAction = Sessions.newSessionAction @User
    action CreateSessionAction = Sessions.createSessionAction @User
    action DeleteSessionAction = Sessions.deleteSessionAction @User

instance Sessions.SessionsControllerConfig User where
    beforeLogin = updateLoginHistory
    afterLoginRedirectPath = "/admin/"

updateLoginHistory user = do
    user
        |> modify #logins (+ 1)
        |> updateRecord
    pure ()