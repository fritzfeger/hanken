module Admin.Controller.Posts where

import Admin.Controller.Prelude
import Admin.View.Posts.Index
import Admin.View.Posts.New
import Admin.View.Posts.Edit
import Admin.View.Posts.Show
import qualified Text.MMark as MMark


instance Controller PostsController where
    beforeAction = ensureIsUser

    action PostsAction = do
        let postsProduct = paramOrNothing @Product "product"
        (postsQ, pagination) <- query @Post
            |> orderByDesc #createdAt
            |> paginate

        posts <- case postsProduct of
            Just postsProduct -> postsQ
                |> filterWhere (#product, postsProduct)
                |> fetch
            Nothing -> postsQ
                |> fetch
        render IndexView { .. }

    action NewPostAction = do
        let post = newRecord
        render NewView { .. }

    action ShowPostAction { postId } = do
        post <- fetch postId
        render ShowView { .. }

    action EditPostAction { postId } = do
        post <- fetch postId
        render EditView { .. }

    action UpdatePostAction { postId } = do
        post <- fetch postId
        post
            |> buildPost
            |> ifValid \case
                Left post -> render EditView { .. }
                Right post -> do
                    post <- post |> updateRecord
                    setSuccessMessage "Post updated"
                    redirectTo EditPostAction { .. }

    action CreatePostAction = do
        let post = newRecord @Post
        post
            |> buildPost
            |> ifValid \case
                Left post -> render NewView { .. }
                Right post -> do
                    post <- post |> createRecord
                    setSuccessMessage "Post created"
                    redirectTo PostsAction

    action DeletePostAction { postId } = do
        post <- fetch postId
        deleteRecord post
        setSuccessMessage "Post deleted"
        redirectTo PostsAction

buildPost post = post
    |> fill @["title","imgFilename","excerpt","product","body","createdAt","visitCount"]
    |> validateField #title nonEmpty
    |> validateField #excerpt nonEmpty
    |> validateField #body nonEmpty
    |> validateField #body isMarkdown


isMarkdown :: Text -> ValidatorResult
isMarkdown text =
    case MMark.parse "" text of
        Left _ -> Failure "is not valid Markdown"
        Right _ -> Success
