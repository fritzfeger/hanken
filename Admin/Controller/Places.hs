module Admin.Controller.Places where

import Admin.Controller.Prelude
import Admin.View.Places.Index
import Admin.View.Places.New
import Admin.View.Places.Edit
import Admin.View.Places.Show

instance Controller PlacesController where
    beforeAction = ensureIsUser

    action PlacesAction = do
        places <- query @Place
            |> orderBy #saison
            |> orderBy #product
            |> orderBy #name
            |> fetch
        render IndexView { .. }

    action NewPlaceAction = do
        let place = newRecord
        render NewView { .. }

    action ShowPlaceAction { placeId } = do
        place <- fetch placeId
        render ShowView { .. }

    action EditPlaceAction { placeId } = do
        place <- fetch placeId
        render EditView { .. }

    action UpdatePlaceAction { placeId } = do
        place <- fetch placeId
        place
            |> buildPlace
            |> ifValid \case
                Left place -> render EditView { .. }
                Right place -> do
                    place <- place |> updateRecord
                    setSuccessMessage "Place updated"
                    redirectTo EditPlaceAction { .. }

    action CreatePlaceAction = do
        let place = newRecord @Place
        place
            |> buildPlace
            |> ifValid \case
                Left place -> render NewView { .. } 
                Right place -> do
                    place <- place |> createRecord
                    setSuccessMessage "Place created"
                    redirectTo PlacesAction

    action DeletePlaceAction { placeId } = do
        place <- fetch placeId
        deleteRecord place
        setSuccessMessage "Place deleted"
        redirectTo PlacesAction

buildPlace place = place
    |> fill @["name","imgFilename","location","description","saison","hours","product","placeType"]
