module Admin.View.Layout (defaultLayout, tableLayout, Html) where

import IHP.ViewPrelude
import IHP.Environment
import qualified Text.Blaze.Html5            as H
import qualified Text.Blaze.Html5.Attributes as A
import Generated.Types
import IHP.Controller.RequestContext
import Admin.Types
import Admin.Routes
import Application.Helper.View


defaultLayout :: Html -> Html
defaultLayout inner = H.docTypeHtml ! A.lang "de" $ [hsx|
<head>
    <title>{pageTitleOrDefault hankenTitle}</title>
    {metaThisPage}
    {metaTags}
    {stylesheets}
    {scriptsHead}
</head>
<body>
    <div class=".container-fluid m-2">
        {navbar}
    </div>
    <div class="container mt-2">
        {renderFlashMessages}
        {inner}
    </div>
    {scriptsBottom}
</body>
|]

tableLayout :: Html -> Html
tableLayout inner = H.docTypeHtml ! A.lang "de" $ [hsx|
<head>
    <title>{pageTitleOrDefault hankenTitle}</title>
    {metaThisPage}
    {metaTags}
    {stylesheets}
    {scriptsHead}
</head>
<body>
    <div class=".container-fluid m-2">
        {navbar}
        {renderFlashMessages}
        {inner}
    </div>
    {scriptsBottom}
</body>
|]

-- ELEMENTS

navbar :: Html
navbar = [hsx|
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="/">{renderBrandMenu}</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href={PostsAction}>Aktuelles</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href={PlacesAction}>Verkaufsstellen</a>
      </li>
    </ul>
    {loginLogoutButton}
  </div>
</nav>
|]
    where
        loginLogoutButton :: Html
        loginLogoutButton =   
            case currentUserOrNothing of  -- `case fromFrozenContext @(Maybe User) of` works too
                Just user -> [hsx|
                    Du bist eingeloggt als {get #email user} - &nbsp;
                    <a class="js-delete js-delete-no-confirm" href={DeleteSessionAction}>Logout</a><br>
                    |]
                Nothing -> [hsx|
                    Please log in first:
                    <a href={NewSessionAction}>Login</a><br>
                    |]

-- META

metaThisPage :: Html
metaThisPage = [hsx| 
    {descriptionOrDefault hankenMissionStatement}
    {ogTitleOrDefault hankenTitle}
    {ogTypeOrDefault "article"}
    {ogDescriptionOrDefault hankenMissionStatement}
    {ogUrl}
    {ogImage}
|]
