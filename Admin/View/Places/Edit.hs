module Admin.View.Places.Edit where
import Admin.View.Prelude

data EditView = EditView { place :: Place }

instance View EditView where
    html EditView { .. } = [hsx|
        {breadcrumb}
        <h1>Verkaufsstelle bearbeiten</h1>
        {renderForm place}
    |]
        where
            breadcrumb = renderBreadcrumb
                [ breadcrumbLink "Verkaufsstellen" PlacesAction
                , breadcrumbText "Verkaufsstelle bearbeiten"
                ]

renderForm :: Place -> Html
renderForm place = formFor place [hsx|
    {submitButton {label = "Speichern"}} &nbsp; 
    <a href={ShowPlaceAction (get #id place)} class="btn btn-secondary">Anzeigen</a>&nbsp;
    <a href={renderOpenTopoMapUrl} class="btn btn-secondary" target="_blank">Open Topo Map anzeigen für Screenshot</a>

    {(textField #name) {required = True}}
    {(textField #imgFilename) {fieldLabel = "Dateiname Bild-Datei"} {placeholder = "Format..."}}
    {(textField #location) {fieldLabel = "Koordinaten"} {placeholder = "Syntax <52.048,8.8842> wie per Rechtsklick von Google Maps kopierbar"}}
    {(textareaField #description) {fieldLabel = "Beschreibung"} { placeholder = "Markdown benutzen, ![{img_alt}](/img/logo/{Dateiname}.{Endung})" }}
    {(selectField #saison saisons)}
    {(textField #hours) {required = True} {fieldLabel = "Öffnungszeiten"} { placeholder = "Markdown benutzen, Tabelle..." }}
    {(selectField #product products) { required = True } {fieldLabel = "Produkt"}}
    {(selectField #placeType placeTypes) { required = True } {fieldLabel = "Verkaufsstellen-Typ"}}
    {submitButton {label = "Speichern"}} &nbsp; <a href={ShowPlaceAction (get #id place)} class="btn btn-secondary">Anzeigen</a>
|]
    where
        renderOpenTopoMapUrl :: Text
        renderOpenTopoMapUrl =
                "https://opentopomap.org/#marker=17/"
            <> show (x xy)
            <> "/"
            <> show (y xy)
            where
                xy = get #location place

products :: [Product]
products = allEnumValues @Product

instance CanSelect Product where
    type SelectValue Product = Product
    selectValue value = value
    selectLabel = tshow

placeTypes :: [PlaceType]
placeTypes = allEnumValues @PlaceType

instance CanSelect PlaceType where
    type SelectValue PlaceType = PlaceType
    selectValue value = value
    selectLabel = tshow

saisons :: [Saison]
saisons = allEnumValues @Saison

instance CanSelect Saison where
    type SelectValue Saison = Saison
    selectValue value = value
    selectLabel = tshow