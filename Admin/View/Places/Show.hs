module Admin.View.Places.Show where
import Admin.View.Prelude

data ShowView = ShowView { place :: Place }

instance View ShowView where
    html ShowView { .. } = [hsx|
        <img src={imgFilePath} style="width:100%">
        {breadcrumb}
        <a href={EditPlaceAction (get #id place)} class="btn btn-secondary" target="_blank">bearbeiten</a>

        <h1>{get #name place}</h1>
        <p>Geo-Location: {get #location place}</p>
        <p>Beschreibung: {get #description place}</p>
        <p>Öffnungszeiten: {get #hours place}</p>
        <p>Produkt: {get #product place}</p>
        <p>Verkaufsstellen-Typ: {get #placeType place}</p>
        <p>Saison-Status: {get #saison place}</p>

    |]
        where
            breadcrumb = renderBreadcrumb
                [ breadcrumbLink "Verkaufsstellen" PlacesAction
                , breadcrumbText [hsx|{get #name place}|]
                ]

            imgFilePath :: Text
            imgFilePath = "../img/place/" <> get #imgFilename place