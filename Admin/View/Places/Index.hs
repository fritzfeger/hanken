module Admin.View.Places.Index where
import Admin.View.Prelude
import Admin.View.Prelude (Place'(placeType))

data IndexView = IndexView { places :: [ Place ]  }

instance View IndexView where
    beforeRender view = do
        setLayout tableLayout
        
    html IndexView { .. } = [hsx|
        <h1>Verkaufsstellen<a href={pathTo NewPlaceAction} class="btn btn-primary ml-4">+ Neu</a></h1>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Verkaufsstelle</th>
                        <th></th>
                        <th>Beschreibung</th>
                        <th>Saison</th>
                        <th>Öffnungszeiten</th>
                        <th>Produkt</th>
                        <th>Verkaufsstellen-Typ</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>{forEach places renderPlace}</tbody>
            </table>
            
        </div>
    |]
        where
            breadcrumb = renderBreadcrumb
                [ breadcrumbLink "Places" PlacesAction
                ]

renderPlace :: Place -> Html
renderPlace place = [hsx|
    <tr>
        <td><a href={ShowPlaceAction (get #id place)}>{get #name place}</a></td>
        <td><a href={EditPlaceAction (get #id place)} class="text-muted">bearbeiten</a></td>
        <td>{get #description place |> renderMarkdown}</td>
        <td>{saisonStatusBadge (get #saison place)}</td>
        <td>{get #hours place |> renderMarkdown}</td>
        <td>{productCustom (get #product place)}</td>
        <td>{get #placeType place}</td>
        <td><a href={DeletePlaceAction (get #id place)} class="js-delete text-muted">löschen</a></td>
    </tr>
|]
        
