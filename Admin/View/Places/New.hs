module Admin.View.Places.New where
import Admin.View.Prelude

data NewView = NewView { place :: Place }

instance View NewView where
    html NewView { .. } = [hsx|
        {breadcrumb}
        <h1>Neue Verkaufsstellen</h1>
        {renderForm place}
    |]
        where
            breadcrumb = renderBreadcrumb
                [ breadcrumbLink "Verkaufsstellen" PlacesAction
                , breadcrumbText "Neue Verkaufsstelle"
                ]

renderForm :: Place -> Html
renderForm place = formFor place [hsx|
    {submitButton {label = "Speichern"}} &nbsp; <a href={ShowPlaceAction (get #id place)} class="btn btn-ff">Anzeigen</a>
    {(textField #name) {required = True}}
    {(textField #imgFilename) {fieldLabel = "Dateiname Bild-Datei"} {placeholder = "Format..."}}
    {(textField #location) {fieldLabel = "Koordinaten"} {placeholder = "Syntax <52.048,8.8842> wie per Rechtsklick von Google Maps kopierbar"}}
    {(textareaField #description) {fieldLabel = "Beschreibung"} { placeholder = "Markdown benutzen, ![{img_alt}](/img/logo/{Dateiname}.{Endung})" }}
    {(selectField #saison saisons)}
    {(textField #hours) {required = True} {fieldLabel = "Öffnungszeiten"} { placeholder = "Markdown benutzen, Tabelle..." }}
    {(selectField #product products) { required = True } {fieldLabel = "Produkt"}}
    {(selectField #placeType placeTypes) { required = True } {fieldLabel = "Verkaufsstellen-Typ"}}
    {submitButton {label = "Speichern"}} &nbsp; <a href={ShowPlaceAction (get #id place)} class="btn btn-secondary">Anzeigen</a>
|]

products :: [Product]
products = allEnumValues @Product

instance CanSelect Product where
    type SelectValue Product = Product
    selectValue value = value
    selectLabel = tshow

placeTypes :: [PlaceType]
placeTypes = allEnumValues @PlaceType

instance CanSelect PlaceType where
    type SelectValue PlaceType = PlaceType
    selectValue value = value
    selectLabel = tshow

saisons :: [Saison]
saisons = allEnumValues @Saison

instance CanSelect Saison where
    type SelectValue Saison = Saison
    selectValue value = value
    selectLabel = tshow