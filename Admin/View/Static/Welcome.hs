module Admin.View.Static.Welcome where
import Admin.View.Prelude

data WelcomeView = WelcomeView

instance View WelcomeView where
    html WelcomeView = [hsx|
        <div>
            <h2>Hallo liebe Hankens!</h2>
            <p>
                Das ist hier das „Backend“, d.h. ein Bereich nur für „Redakteure“. Wenn Ihr die Seite so sehen wollt wie die normalen Besucher, klickt oben links auf das Logo. Um wieder hierher zurückzukommen, im Browser hanken-entrup.de/admin/ aufrufen (Schrägstrich am Ende nicht vergessen!).
            </p>
            <p>
                Um neue Blog-Beiträge zu erstellen oder neue Verkaufsstellen anzulegen oder bestehende zu bearbeiten, bitte oben im Menü das Passende auswählen! Einen Bilder-Upload gibt es hier vorerst nicht, d.h. zunächst müsstet Ihr mir Bilder zuschicken, und ich lege dann den Eintrag an. Die Startseite und die Produktseiten muss ebenfalls ich ändern...
            </p>
        </div>
|]