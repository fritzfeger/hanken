module Admin.View.Posts.Edit where
import Admin.View.Prelude

data EditView = EditView { post :: Post }

instance View EditView where
    html EditView { .. } = [hsx|
        {breadcrumb}
        <h1>Beitrag bearbeiten</h1>
        {renderForm post}
    |]
        where
            breadcrumb = renderBreadcrumb
                [ breadcrumbLink "Aktuelles" PostsAction
                , breadcrumbText "Beitrag bearbeiten"
                ]

renderForm :: Post -> Html
renderForm post = formFor post [hsx|
    {(textField #title)}
    {(dateField #createdAt) {fieldLabel = "Date", fieldValue = createdAtValue}}
    {(textField #imgFilename) {placeholder="Namensformat <hanken-entrup-be-zeich-nung>_<BreitePx>.<Endung>, erlaubte Formate jpg, jpeg, png, tiff, gif, svg"}}
    {(textField #excerpt)}
    {(selectField #product products) {fieldLabel = "Produkt bzw. Thema"}}
    {submitButton {label="Speichern"}} &nbsp; <a href={ShowPostAction (get #id post)} class="btn btn-secondary">Anzeigen</a>
    {(textareaField #body) {placeholder="Markdown benutzen, ![{img_alt}](/img/logo/{Dateiname}.{Endung})" }}
    {submitButton {label="Speichern"}} &nbsp; <a href={ShowPostAction (get #id post)} class="btn btn-secondary">Anzeigen</a>

|]
    where
        createdAtValue = post
            |> get #createdAt 
            |> formatTime defaultTimeLocale "%Y-%m-%d"
            |> cs

products :: [Product]
products = allEnumValues @Product

instance CanSelect Product where
    type SelectValue Product = Product
    selectValue value = value
    selectLabel = tshow
