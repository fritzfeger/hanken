module Admin.View.Posts.Index where
import Admin.View.Prelude

data IndexView = IndexView { posts :: [ Post ] , pagination :: Pagination }

instance View IndexView where
    beforeRender view = do
        setLayout tableLayout

    html IndexView { .. } = [hsx|
        <h1>Aktuelles<a href={pathTo NewPostAction} class="btn btn-primary ml-4">+ Neu</a></h1>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Titel</th>
                        <th></th>
                        <th>Thema</th>
                        <th>Datum</th>
                        <th>Bilddatei</th>
                        <th>Exzerpt</th>
                        <th>Aufrufe</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>{forEach posts renderPost}</tbody>
            </table>
            {renderPagination pagination}
        </div>
    |]
        where
            breadcrumb = renderBreadcrumb
                [ breadcrumbLink "Aktuelles" PostsAction
                ]

renderPost :: Post -> Html
renderPost post = [hsx|
    <tr>
        <td><a href={ShowPostAction (get #id post)}>{get #title post}</a></td>
        <td><a href={EditPostAction (get #id post)} class="text-muted">bearbeiten</a></td>
        <td>{productCustom (get #product post)}</td>
        <td>{get #createdAt post |> date}</td>
        <td>{get #imgFilename post}</td>
        <td>{get #excerpt post}</td>
        <td>{get #visitCount post}</td>
        <td><a href={DeletePostAction (get #id post)} class="js-delete text-muted">löschen</a></td>
    </tr>
|]