module Admin.View.Posts.New where
import Admin.View.Prelude
import Data.Time.Calendar

data NewView = NewView { post :: Post }

instance View NewView where
    html NewView { .. } = [hsx|
        {breadcrumb}
        <h1>Neuer Beitrag</h1>
        {renderForm post}
    |]
        where
            breadcrumb = renderBreadcrumb
                [ breadcrumbLink "Aktuelles" PostsAction
                , breadcrumbText "Neuer Beitrag"
                ]

renderForm :: Post -> Html
renderForm post = formFor post [hsx|
    {(textField #title)}
    {(dateField #createdAt) {fieldLabel = "Datum"}{ placeholder = "Datum eingeben, auch wenn heute" }}
    {(textField #imgFilename) {placeholder="Namensformat <hanken-entrup-be-zeich-nung>_<BreitePx>.<Endung>, erlaubte Formate jpg, jpeg, png, tiff, gif, svg"}}
    {(textField #excerpt)}
    {(selectField #product products) {fieldLabel = "Produkt bzw. Thema"}}
    {submitButton {label = "Speichern"}}
    {(textareaField #body) {placeholder="Markdown benutzen, ![{img_alt}](/img/logo/{Dateiname}.{Endung})" }}
    {submitButton {label = "Speichern"}}
|]
    where
        products :: [Product]
        products = allEnumValues @Product

instance CanSelect Product where
    type SelectValue Product = Product
    selectValue value = value
    selectLabel = tshow