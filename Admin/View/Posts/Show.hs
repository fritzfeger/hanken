module Admin.View.Posts.Show where
import Admin.View.Prelude

data ShowView = ShowView { post :: Post }

instance View ShowView where
    html ShowView { .. } = [hsx|
        {breadcrumb}
        <h1>{get #title post}</h1>
         <table>
            <tr>
                <td>Exzerpt:</td>
                <td colspan="3">{get #excerpt post}</td>
            </tr>
            <tr>
                <td style="width:120px">Kategorie:</td>
                <td style="width:220px"><br> <!-- Kategorie = gefilterte Liste verlinken --></td>
                <td style="width:120px">imgFilename:</td>
                <td>{get #imgFilename post}</td>
            </tr>
            <tr>
                <td>Datum:</td>
                <td>{get #createdAt post |> date}<br></td>
                <td>Aufrufe:</td>
                <td>{get #visitCount post}<br></td>
            </tr>
            <tr>
                <td><a href={EditPostAction (get #id post)} class="btn btn-primary">bearbeiten</a></td>
                <td><a href={PostsAction} class="btn btn-secondary">Index</a></td>
                <td></td>
                <td><br></td>
            </tr>
        </table>
        <div class="post-body">{get #body post |> renderMarkdown}</div>
        <p>
            <a href={EditPostAction (get #id post)} class="btn btn-primary">bearbeiten</a>&nbsp;
            <a href={PostsAction} class="btn btn-secondary">Index</a>
        </p>
    |]
        where
            breadcrumb = renderBreadcrumb
                [ breadcrumbLink "Aktuelles" PostsAction
                , breadcrumbText [hsx|{get #title post}|]
                ]