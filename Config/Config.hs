module Config where

import IHP.Prelude
import IHP.Environment
import IHP.FrameworkConfig
import IHP.EnvVar

config :: ConfigBuilder
config = do
    appEnvironment <- envOrNothing "APP_ENVIRONMENT"
    let appMode = case appEnvironment of 
                    Nothing -> Development
                    Just ("PRODUCTION" :: Text) -> Production
                    _ -> error "Unrecognized App Environment"
    option appMode
    appHostName <- envOrDefault "APP_HOST_NAME" "localhost"
    option (AppHostname appHostName)
    -- See https://ihp.digitallyinduced.com/Guide/config.html
    -- for what you can do here
