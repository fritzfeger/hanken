module Web.View.Post.Show where
import Web.View.Prelude
import Admin.Controller.Prelude (renderHtml, remainingText)
import qualified Data.Attoparsec.Text as P -- wie Parser
import Data.Attoparsec.Text

data ShowView = ShowView { post :: Post }

instance View ShowView where
    html ShowView { .. } = [hsx|
        <div class="image-container">     
            <div style="--aspect-ratio:3/1;">
                <picture>
                    <source media={imgMinWidthBig} srcset={imgFilePathBig}/>
                    <source media={imgMinWidthMedium} srcset={imgFilePathMedium}/>
                    <img src={imgFilePathSmall} alt={get #imgFilename post}/>
                </picture>
            </div>
        </div>

        {breadcrumb}
        
        <h1>{get #title post}</h1>
        <p>{get #createdAt post |> date} | {productCustom (get #product post)}</p>
        <hr>
        <div class="post-body">{get #body post |> renderMarkdown}</div>
    |]
        where
            breadcrumb = renderBreadcrumb
                [ breadcrumbLinkExternal "Hanken Entrup" "/"
                , breadcrumbLink "Aktuelles" PostsAction
                , breadcrumbText [hsx|{get #title post}|]
                ]

            imgFilePathBig :: Text
            imgFilePathBig = "../img/post/" <> get #imgFilename post <> "_" <> show (filesize imgBig) <> ".webp"

            imgFilePathMedium :: Text
            imgFilePathMedium = "../img/post/" <> get #imgFilename post <> "_" <> show (filesize imgMedium) <> ".webp"

            imgFilePathSmall :: Text
            imgFilePathSmall = "../img/post/" <> get #imgFilename post <> "_" <> show (filesize imgSmall) <> ".webp"

            

runParserImgFileName :: Text -> (Text, Text, Text)
runParserImgFileName imgFileName = case parseOnly parseImgFileName imgFileName of
    Right imgInfoTuple -> imgInfoTuple
    Left  _            -> ("-", "-", "-")

parseImgFileName :: Parser (Text, Text, Text)
parseImgFileName = do
    designation <- cs <$> manyTill anyChar (string "_")
    filesize    <- cs <$> manyTill anyChar (string ".")
    extension   <- string "webp" <|> string "jpg" <|> string "jpeg" <|> string "png" <|> string "tiff" <|> string "svg" <|> string "gif"
    pure (designation, filesize, extension)

{-
Bilder komprimieren: https://squoosh.app/: 1920 (~230kb), 1200 (~180kb), 800 (60-80kb), webp, quality: 85+
-}