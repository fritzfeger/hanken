module Web.View.Post.Index where
import Web.View.Prelude

data IndexView = IndexView 
    { post :: [ Post ] 
    , pagination :: Pagination
    , postsProduct :: Maybe Product
    }

instance View IndexView where
    html IndexView { .. } = [hsx|
        {renderImgFullWidth "hanken-entrup-kartoffeln-roder-weit"}
        <h1>Aktuelles</h1>
        <p>
            <a href={PostsAction} role="button" 
                class={classes ["btn btn-secondary", ("selected", isActivePath PostsAction)]}>Alle Themen</a>
            {forEach (allEnumValues @Product) renderProduct}
        </p>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Titel</th>
                        <th>Thema</th>
                        <th>Datum</th>
                    </tr>
                </thead>
                <tbody>{forEach post renderPost}</tbody>
            </table>
            {renderPagination pagination}
        </div>
    |]
        where
            renderProduct product = [hsx|
                <a href={produktPathQuery product} role="button" 
                    class={classes ["btn btn-secondary", ("selected", isProduktPath)]}>{productCustom product}</a>&nbsp;
            |]
                where
                    produktPathQuery product = "/Posts?product=" <> show product |> toLower
                    isProduktPath = isActivePath (produktPathQuery product)

renderPost :: Post -> Html
renderPost post = [hsx|
    <tr>
        <td><a href={ShowPostAction (get #id post)}>{get #title post}</a></td>
        <td>{productCustom (get #product post)}</td>
        <td>{get #createdAt post |> date}</td>
    </tr>
|]
