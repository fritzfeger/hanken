module Web.View.Static.Nachhaltigkeit where
import Web.View.Prelude
data NachhaltigkeitView = NachhaltigkeitView

instance View NachhaltigkeitView where
    html NachhaltigkeitView = [hsx|
        {renderImgFullWidth "hanken-entrup-hof-blumen"}
        <div>
            <h1>
                Nachhaltigkeit
            </h1>
            <p>
                Es gibt einen Aspekt von Nachhaltigkeit, bei dem kein trendiges und noch so grünes StartUp-Unternehmen einem landwirtschaftlichen Familienbetrieb das Wasser reichen kann: Seit hunderten von Jahren hat die jeweilige Eigentümer-Generation den Hof nicht verkauft, um mit dem Erlös ein Leben in Saus und Braus zu führen oder etwas ganz Neues zu probieren, sondern diesen Schatz bewahrt und kontinuierlich weiterentwickelt für die kommenden Generationen.
            </p>
            <p>
                Wer Bäume pflanzt, tut das für seine Urenkel. Landwirte denken in sehr, sehr langen Zeiträumen. Dieses Denken prägt auch unseren Umgang mit unseren natürlichen Ressourcen.
            </p>
            <p>&nbsp;</p>
        </div>
        {renderImgFullWidth "hanken-entrup-kartoffeln-roder-unten"}
        <div>
            <h2>
                Unser Boden
            </h2>
            <p>
                Warum hier ein Foto eines tonnenschweren Ackerschleppers mit ebensoschwerem Kartoffelroder? Wir wissen, dass der Boden unser wertvollstest Gut ist, und behandeln ihn daher sehr pfleglich. Sehen Sie auf dem Bild die dünnen Rohre, die von der Schlepperachse zu den Reifen führen? Damit lassen wir, immer, wenn wir von der Straße kommen, Luft von den Reifen, um das Gewicht der Maschinen möglichst breit zu verteilen und so möglichst wenig Druck auf den Boden auszuüben. Wenn es zu naß ist, befahren und bearbeiten wir den Acker gar nicht.
            </p>
            <p>
                Für den Humusaufbau führen wir dem Boden organische Substanz zu, indem wir zusätzlich zu unserem Pferdemist noch Gärrest und Gülle von befreundeten Betrieben als natürlichen Dünger verwenden. Diese verwenden wiederum Früchte unserer Felder als Futter für ihre Tiere bzw. Biogasanlage. So entsteht ein lokaler Nährstoff-Kreislauf.
            </p>
            <p>
                Jährlich werden Bodenproben gezogen und analysiert, um das Düngen der Pflanzen so genau und bedarfsorientiert wie möglich zu gestalten. Dies bedeutet, dass nur die Nährstoffe dem Boden wieder zurückgegeben werden, die die Teile der Pflanze ihm entziehen, die auf Ihrem Tisch landen.
            </p>
            <h2>
                Pflanzenschutz
            </h2>
            <p>
                Wir wollen nicht ganz auf chemischen Pflanzenschutz verzichten. Bevor die Kraut- und Knollenfäule oder der Kartoffelkäfer unsere wertvolle Ernte vernichtet, greifen wir ein. Immer in Absprache mit Beratern, minimal, den Grundsätzen des integrierten Pflanzenbaus und dem Schadschwellenprinzip folgend und selbstverständlich unter Einhaltung der Wartezeiten vor der Ernte.
            </p>
            <p>&nbsp;</p>
        </div>
        {renderImgFullWidth "hanken-entrup-kartoffeln-roder-sortieren"}
        <div>
            <h2>
                Festangestellte Mitarbeiter und Saisonarbeitskräfte
            </h2>
            <p>
                Mißstände in Entlohnung, Unterbringung und Arbeitsbedingungen von Saisonarbeitskräften sind in unserer Branche leider immer wieder zu beklagen. Wir streben demgegenüber bei unseren Erntehelfern nach langfristigen Beziehungen basiernd auf Fairness und Vertrauen. Wir kennen „unsere“ Saisonarbeiter-Familien aus Rumänien, die jedes Frühjahr für 8 Wochen kommen, um uns bei der arbeitsintensiven Erdbeerernte zu unterstützen, seit nunmehr 10 Jahren. Die Erntehelfer wohnen, wenn sie hier bei uns sind, in gemütlichen Wohnungen im Fachwerkhaus mitten auf dem Hof, werden ordentlich bezahlt und haben vernünftige Arbeitszeiten.
            </p>
            <p>
                Die Arbeit mit Kartoffeln ist gleichmäßiger über einen längeren Zeitraum verteilt, das Sortieren und Abpacken sogar ganzjährig. Dafür reist niemand an, das machen wir mit regional ansässigen Arbeitskräften. Wir haben bis zu vier Festangestellte, teilweise als Auszubildende – genau, wir bilden aus, auch das ist Nachhaltigkeit!
            </p>
            <h2>
                Erneuerbare Energien
            </h2>
            <p>
                Ungefähr die selbe Strommenge, die wir verbrauchen, produzieren wir mit unserer Photovoltaik-Anlage. Die Dächer der Wirtschaftsgebäude, die schon unsere Vorfahren errichtet haben, verfügen glücklicherweise über eine gute Süd-Ausrichtung und den passenden Neigungswinkel, so dass sie sich für eine Solaranlage anbieten.
            </p>
            <p>&nbsp;</p>
        </div>
        {renderImgFullWidth "hanken-entrup-familie-roder"}
        <div>
            <h2>
                Die nächste Generation
            </h2>
            <p>
                Die nächste Generation steht schon in den Startlöchern!
            </p>
        </div>
        <div class="jumbotron">
            <h2>Interesse?  </h2>
            <p class="lead">Wenn Sie was wissen wollen, wie wir das so machen mit der Nachhaltigkeit...</p>
            <div class="row row-cols-1 row-cols-md-2 mb-2">
                <div class="col mb-4 br-4">
                    <div class="card">
                        <h3>Wir freuen uns über Ihre Kontaktaufnahme!</h3>
                        <p>
                            Jens Hanken<br>
                            Entruper Weg 194<br>
                            32657 Lemgo<br>
                            Telefon 05261 4146<br>
                            <a href = "mailto:info@hanken-entrup.de">info@hanken-entrup.de</a>
                        </p>
                    </div>
                </div>
                <div class="col mb-4">
                    <div class="card">
                        <h3>{hankenWasAktuellProdukt}</h3>
                        <p>Was wir in Sachen Nachhaltigkeit tun, wenn wir etwas ändern, wenn wir Gedanken auch zu Dingen außerhalb unseres Betriebs teilen wollen:</p>
                        <a class="btn btn-secondary" href="/Posts?product=nachhaltigkeit" role="button">Aktuelles über Nachhaltigkeit</a>
                    </div>
                </div>
            </div>
        </div>
        |]
