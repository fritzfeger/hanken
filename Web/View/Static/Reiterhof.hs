module Web.View.Static.Reiterhof where
import Web.View.Prelude
data ReiterhofView = ReiterhofView

instance View ReiterhofView where
    html ReiterhofView = [hsx|
        {renderImgFullWidth "hanken-entrup-pferd-schimmel"}
        <div>
            <h1>Reiterhof</h1>
            <p>
                Seit vielen Jahren sind auf unserem Hof Pferde zu Hause. Nach und nach haben wir alte Ställe in den Fachwerkhäusern zu adäquaten Pferdeboxen umgebaut, in denen sich die Vierbeiner richtig wohlfühlen können. Es stehen einige Pensionspferdeboxen mit angrenzenden Paddocks zur Verfügung.
            </p>
            <p>
                Eine kleine Reithalle, Weide im Sommer und ein Winterauslauf sind vorhanden. Unser neuer Reitplatz mit Sandboden ist 20 x 40 m groß und komplett drainiert. Ein weitläufiges und landschaftlichen reizvolles Ausreitgelände im Lemgoer Stadtwald ist von Entrup aus zu Pferd schnell erreichbar. 
            </p>
        </div>
        <div class="jumbotron">
            <h2>Interesse?  </h2>
            <p class="lead">Wenn Sie ein Pferd bei uns einstellen möchten</p>
            <div class="row row-cols-1 row-cols-md-2 mb-2">
                <div class="col mb-4 br-4">
                    <div class="card">
                        <h3>Wir freuen uns über Ihre Kontaktaufnahme!</h3>
                        <p>
                            Jens Hanken<br>
                            Entruper Weg 194<br>
                            32657 Lemgo<br>
                            Telefon 05261 4146<br>
                            <a href = "mailto:info@hanken-entrup.de">info@hanken-entrup.de</a>
                        </p>
                    </div>
                </div>
                <div class="col mb-4">
                    <div class="card">
                        <h3>{hankenWasAktuellProdukt}</h3>
                        <p>Wenn die Saison beginnt, wir eine neue Verkaufsstelle eröffnen, sich im Sortiment etwas tut oder es auf dem Betrieb etwas zu berichten gibt:</p>
                        <a class="btn btn-secondary" href="/Posts" role="button">Aktuelles</a>
                    </div>
                </div>
            </div>
        </div>
        |]