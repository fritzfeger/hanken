module Web.View.Static.Kartoffeln where
import Web.View.Prelude
data KartoffelnView = KartoffelnView

instance View KartoffelnView where
    html KartoffelnView = [hsx|
        {renderImgFullWidth "hanken-entrup-kartoffeln-ausgraben"}
        <div>
            <h1>Kartoffeln</h1>
            <p>
                Seit 2004 bauen wir auf unseren Feldern rund um Entrup für Sie Kartoffeln an. Geschmacklich vorzüglich und für alle Kartoffelgerichte geeignet, lassen sie sich dank ihrer schönen hellen Schale auch ungeschält verarbeiten. Hier nur einige Ideen, um Ihnen den Mund wässrig zu machen, was man mit dem „Ackergold“ anstellen kann:
            </p>
            <ul>
                <li>puristisch mit Butter und Salz</li>
                <li>Salzkartoffeln mit Petersilie als Beilage</li>
                <li>Kartoffelbrei mit Muskat, einem Hauch Knoblauch und brauner Butter</li>
                <li>Kartoffelgratin mit cremiger Sahne und krossem, würzigem Käse</li>
                <li>Bratkartoffeln mit geröstetem Speck</li>
                <li>Pellkartoffeln mit Sahnehering</li>
                <li>Ofenkartoffeln mit Kräuterquark</li>
                <li>Ofenkartoffeln mit Olivenöl und Rosmarin</li>
                <li>Kartoffeldressing zum Salat: einfach zwei Löffel übriggebliebenen Kartoffelbrei dazu</li>
                <li>Chips oder Wedges aus der Friteuse von ungeschälten Kartoffeln</li>
                <li>A propos Friteuse: selbstgemachte Kroketten!</li>
                <li>Schweizer Rösti mit Räucherlachs</li>
                <li>Kartoffelpuffer mit Apfelmus</li>
                <li>Lippischer Pickert</li>
                <li>Spanische Tortilla</li>
                <li>Kartoffelsuppe, Kartoffeleintopf</li>
                <li>Kartoffelsalat mit Mayonnaise oder Öl oder Brühe</li>
                <li>... und viele, viele mehr!</li>
            </ul>
            <h2>Sorten und Anbau</h2>
            <p>
                Wir haben Vieles ausprobiert und für uns entschieden, daß wir den „Hype“ um Kartoffelsorten nicht mitmachen. Vom Anbau über die Lagerung über die Küche bis auf den Teller sind unsere Favoriten Belana und Marabell. Alle oben aufgeführten Kartoffelgerichte kann damit ohne Einschränkungen zubereiten!
            </p>
            <p>
                Eine Beschreibung der beiden Sorten und unsere Begründung ist in diesem Artikel im Blog nachzulesen:
            </p>
            <ul>
                <li><a href="../ShowPost?postId=316674b0-07e7-4cfd-b4fd-021d8ec03daf">Unsere Kartoffelsorten</a></li>
            </ul>
            <p>
                Falls Sie sich dafür interessieren, wie wir möglichst bodenschonend arbeiten, bedarfsorientiert düngen, nur wenn nötig spritzen, und was wir sonst noch für die Natur tun:
            </p>
            <ul>
                <li><a href="../nachhaltigkeit">Nachhaltigkeit</a></li>
            </ul>
            <h2>Sortierung und Lagerung</h2>
            <p>
                Wir roden die Kartoffeln im Herbst direkt in spezielle Kisten, die unmittelbar nach der Ernte ins Lager gestellt werden. Unser große Kartoffelhalle ist ein Belüftungslager; die kleine Halle können wir auf 4 Grad herunterkühlen und so die alterntige Ware bis in den Juni hinein keimfrei halten. Dies alles ist möglich ohne jeglichen Einsatz chemischer Hilfsstoffe, Begasung, Bestrahlung etc. – einfach durch ausgeklügelte Belüftungsstrategie!
            </p>
            <p>
                Für Sie bedeutet dies, daß Ihre Kartoffeln besser bei uns im Lager liegen als bei Ihnen im Keller. Wenn Sie immer unverkeimte und noch nicht verschrumpelte Kartoffeln haben wollen, holen Sie am besten immer nur so viel, wie Sie in den nächsten wenigen Wochen verbrauchen. Lagerung zu Hause unbeheizt/kühl, luftig, frostfrei, dunkel und nicht neben Äpfeln.
            </p>
            <p>
                Aus unseren Lagerhallen bringen wir die Kartoffeln dann kistenweise in die Sortieranlage, und zwar erst dann, wenn sie auch direkt in den Verkauf gehen sollen. Wir sortieren die kostbaren Knollen von Hand, so dass nur schöne Kartoffeln in die Tüte oder in den Sack kommen.
            </p>
            <p>
                Unser Kartoffelhäuschen in Entrup und die unserer Partner-Betriebe sind gut isoliert und beheizt, damit unsere leckeren Kartoffeln auch in der dunklen und kalten Jahreszeit unter optimalen Bedingungen für Sie bereitstehen.
            </p>
            <div class="jumbotron">
                <h2>Appetit auf Kartoffeln?</h2>
                <p class="lead">Hanken Entrup – {hankenMissionStatement}</p>
                <div class="row row-cols-1 row-cols-md-2 mb-2">
                    <div class="col mb-4 br-4">
                        <div class="card">
                            <h3>{hankenWoErhaeltlich}</h3>
                            <p>Ihre Kartoffeln warten auf Sie in ganzjährig und rund um die Uhr geöffneten Selbstbedienungs-Kartoffelhäuschen oder in einem der von uns belieferten Supermärkte!</p>
                            <a class="btn btn-primary" href="/Places?product=kartoffeln" role="button">Kartoffel-Verkaufsstellen</a>
                        </div>
                    </div>
                    <div class="col mb-4">
                        <div class="card">
                            <h3>{hankenWasAktuellProdukt}</h3>
                            <p>Pflanzung, Pflege und Ernte, Änderungen im Sortiment, neue Fotos rund um das „Ackergold“ was es sonst noch Interessantes von unseren Kartoffeln zu erzählen gibt!</p>
                            <a class="btn btn-secondary" href="/Posts?product=kartoffeln" role="button">Aktuelles über Kartoffeln</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        |]