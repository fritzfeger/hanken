module Web.View.Static.Kuerbisse where
import Web.View.Prelude
data KuerbisseView = KuerbisseView

instance View KuerbisseView where
    html KuerbisseView = [hsx|
        {renderImgFullWidth "hanken-entrup-kuerbisse"}
        <div>
            <h1>Kürbisse, Gurken</h1>
            <p>
                Wenn wir hofnah eine passende Fläche frei haben, bauen wir auch im begrenzten Umfang Zier- und Speisekürbisse und Schlangengurken an. Weitere Gemüse könnten dazukommen! Der Verkaufsstand wird dann in der Erntezeit in Entrup gegenüber dem Kartoffelhäuschen aufgebaut.
            </p>
<!-- 
            <div class="jumbotron">
                <h2>Appetit auf Kürbisse?</h2>
                <p class="lead">Hanken Entrup – {hankenMissionStatement}</p>
                <div class="row row-cols-1 row-cols-md-2 mb-2">
                    <div class="col mb-4 br-4">
                        <div class="card">
                            <h3>{hankenWoErhaeltlich}</h3>
                            <p>Freitext Kürbisse – bisher keine Verkaufsstelle in der Liste!</p>
                            <a class="btn btn-primary" href="/Places?product=kürbisse" role="button">Kürbis-Verkaufsstellen</a>
                        </div>
                    </div>
                    <div class="col mb-4">
                        <div class="card">
                            <h3>{hankenWasAktuellProdukt}</h3>
                            <p>Pflanzung, Pflege und Ernte, Änderungen im Sortiment, neue Fotos rund um das vielseitige Gemüse, und was es sonst noch Interessantes von unseren Kürbissen zu erzählen gibt!</p>
                            <a class="btn btn-secondary" href="/Posts?product=kürbisse" role="button">Aktuelles über Kürbisse</a>
                        </div>
                    </div>
                </div>
            </div>
             -->
        </div>
        |]