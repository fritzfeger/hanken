module Web.View.Static.Eier where
import Web.View.Prelude
import Application.Helper.View (hankenWasAktuellProdukt)
data EierView = EierView

instance View EierView where
    html EierView = [hsx|
        {renderImgFullWidth "hanken-entrup-huehnermobil"}
        <div>
            <h1>
                Eier
            </h1>
            <p>
                Unsere 350 Hühner der Rasse „Lohmann Brown“ legen im Schnitt knapp ein Ei pro Tag. Die Eier bringen wir frisch vom Nest in unser Kartoffel-Verkaufshäuschen in Entrup, wo sie täglich rund um die Uhr abgeholt werden können.
            </p>
            <h2>
                Freilandhaltung im Hühnermobil
            </h2>
            <p>
                Unsere Hühner wohnen seit 2013 in einem Hühnermobil Modell <a href="https://www.huehnermobil.de/produkte/huemo-plus-350/">HüMo PLUS 350</a>. Das Hühnermobil ist „autark“, d.h. verfügt über Futter, Wasser und überhaupt alles, was ein Stall braucht. 
            </p>
            <p>
                Der Clou dieses Stalls besteht darin, daß man ihn leicht „umsetzten“ kann: er hat Reifen und eine Deichsel, so daß er sich problemlos auf eine andere Stelle ziehen läßt. Wir tun das jede Woche. Dadurch gibt es für die gefiederten Damen ständig frisches Gras, Scharren und Picken und viel Abwechslung im Sonnenschein. Eine Übernutzung des Bodens durch zu viel Hühnerdung und Zerstörung der Grasnarbe wird ebenso vermieden wie eine Keimansammlung; beides führt bei klassischer Freilandhaltung auf einer Fläche in Dauernutzung zu Problemen. Wir sind der Meinung, daß erst das Hühnermobil echte Freilandhaltung ermöglicht.
            </p>
            <p>
                Sogar in der Stromversorgung ist das Hühnermobil durch seine PV-Module autark! Kameras und Sensoren und Benachrichtigungen auf das Mobiltelefon ermöglichen uns, sofort zu reagieren, wenn einmal etwas nicht in Ordnung sein sollte.
            </p>
            <p>
                An mehreren Stellen, an denen Sie unsere Kartoffeln kaufen können, erhalten Sie ebenfalls Eier aus dem Hühnermobil der jeweiligen Landwirte vor Ort – klicken Sie sich doch einfach mal durch die Liste unserer <a href="../Places?product=kartoffeln">Kartoffel-Verkaufsstellen</a>!
            </p>
            <div class="jumbotron">
                <h2>Appetit auf Eier?</h2>
                <p class="lead">Hanken Entrup – {hankenMissionStatement}</p>
                <div class="row row-cols-1 row-cols-md-2 mb-2">
                    <div class="col mb-4 br-4">
                        <div class="card">
                            <h3>{hankenWoErhaeltlich}</h3>
                            <p>Ihre frischen Eier aus unserem tierwohl-gerechten Hühnermobil warten auf Sie im ganzjährig und rund um die Uhr geöffneten Selbstbedienungs-Kartoffelhäuschen in Entrup!</p>
                            <a class="btn btn-primary" href="/Places?product=eier" role="button">Eier-Verkaufsstellen</a>
                        </div>
                    </div>
                    <div class="col mb-4">
                        <div class="card">
                            <h3>{hankenWasAktuellProdukt}</h3>
                            <p>Welche Hühner bei uns tätig sind, was sie fressen und was wir für ihr Wohlbefinden tun, und was es sonst noch Interessantes von ihnen und den von ihnen gelegten Eiern zu erzählen gibt!</p>
                            <a class="btn btn-secondary" href="/Posts?product=eier" role="button">Aktuelles über Eier</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        |]