module Web.View.Static.Erdbeeren where
import Web.View.Prelude
data ErdbeerenView = ErdbeerenView

instance View ErdbeerenView where
    html ErdbeerenView = [hsx|
        {renderImgFullWidth "hanken-entrup-erdbeer-schalen-feld"}
        <div>
            <h1>Erdbeeren</h1>
            <p>
                Erdbeeren sind unser Traditionsprodukt! Im Jahre 2001 haben wir unseren Erdbeeranbau auf ganz Lippe ausgeweitet, es entstanden Plantagen in Lage-Hardissen, Bad Salzuflen-Ehrsen, Leopoldshöhe und in Hohenhausen (siehe <a href="/Places?product=erdbeeren">Liste der Verkaufsstellen</a>). Hier haben unsere Kunden die Möglichkeit, unsere leckeren Erdbeeren selbst zu pflücken oder am Stand zu kaufen. Weiterhin stehen etliche Verkaufstände in unserer Umgebung, wo Sie unsere leckeren Früchte erwerben können.
            </p>
            <p>
                Aufgrund langjähriger Erfahrung und im ständigen Bemühen um die Optimierung unserer Erntezeit haben wir uns für ein neues Anbauverfahren entschieden: um frühzeitig für unsere Kunden am Markt zu sein, werden auf einigen Feldern Folientunnel über die Reihen gestellt. So können wir bereits ab Anfang Mai die ersten Erdbeeren anbieten.
            </p>
            <h2>Sorten</h2>
            <p>
                Unser Züchter <a href="https://kraege.de/">Kraege Beerenpflanzen</a> aus Telgte (Westfalen) liefert uns jedes Jahr neues Pflanzgut für die Bestandserneuerung. Wir kaufen keine Pflanzen aus dem Ausland, sondern verlassen uns zu 100% auf unseren regionalen Lieferanten!
            </p>
            <p>
                Auf ca. 20% unserer Anbaufläche steht die Erdbeersorte Sonata, an der auch im zweiten Jahr noch schöne Erdbeeren wachsen. Die anderen Sorten liefern bessere Qualität und größere Früchte, wenn man sie jährlich neu pflanzt. Die frühesten Erdbeeren bringen die Sorten Glorielle und Clery, unsere Hauptsorten sind Korona und Sonata, als Spätsorte verwenden wir Malvina. So haben wir von Anfang Mai bis Ende Juni qualitativ beste Erdbeeren. Geschmacklich minderwertige Massensorten mit supermarkt-kompatibler verlängerter Haltbarkeit gibt es bei uns nicht. Also schnell aufessen, so lange sie frisch sind!
            </p>
            <h2>VitaSol Bad Salzuflen: Saisonspecial „Erdbeersommer“ von Mai bis August</h2>
            <img src="../img/logo/VitaSol_Logo.webp" width="50%">
            <p>
                Von Mai bis August ist in der VitaSol-Therme Erdbeersaison. In Kooperation mit uns erwartet alle Erdbeerliebhaber ein besonderer Thermengenuss: Zu jeder gekauften 500-g-Erdbeerschale gibt es einen Gutschein-Coupon der VitaSol-Therme. Für vier erworbene Gutscheine erhalten Sie in der VitaSol-Therme eine Zusatzstunde gratis für die ThermenLandschaft und den SaunaPark. Auch der SaunaPark mit fruchtigen Spezial-Aufgüssen und die WellnessLounge mit wohlduftenden Erdbeer-Aromaölmassagen stimmen thematisch auf den Sommer ein. Mehr zum Erdbeersommer im Vitasol? (Der Link https://www.vitasol.de/de/perm/veranstaltungen/erdbeersommer.html#c4729 ist wohl nicht mehr aktuell)
            </p>
            <div class="jumbotron">
                <h2>Appetit auf Erdbeeren?</h2>
                <p class="lead">Hanken Entrup – {hankenMissionStatement}</p>
                <div class="row row-cols-1 row-cols-md-2 mb-2">
                    <div class="col mb-4 br-4">
                        <div class="card">
                            <h3>{hankenWoErhaeltlich}</h3>
                            <p>Pflücken Sie Ihre Erdbeeren auf unseren Feldern selbst, oder nehmen Sie einfach ein paar Schalen an einem Verkaufsstand oder in einem der von uns belieferten Supermärkte mit!</p>
                            <a class="btn btn-primary" href="/Places?product=erdbeeren" role="button">Erdbeer-Verkaufsstellen</a>
                        </div>
                    </div>
                    <div class="col mb-4">
                        <div class="card">
                            <h3>{hankenWasAktuellProdukt}</h3>
                            <p>Wann die Saison startet, wann Selbstpfückfelder und Stände öffnen und schließen, was es sonst noch Interessantes von unseren Erdbeeren zu erzählen gibt!</p>
                            <a class="btn btn-secondary" href="/Posts?product=erdbeeren" role="button">Aktuelles über Erdbeeren</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    |]