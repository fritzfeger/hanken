module Web.View.Static.Welcome where
import Web.View.Prelude

data WelcomeView = WelcomeView

-- {renderImgFullWidth "hanken-entrup-kuerbis-fachwerk"}
instance View WelcomeView where
    html WelcomeView = [hsx|
        {renderImgFullWidth "hanken-entrup-erdbeer-schalen-feld"}
        <div>
            <h1>{hankenMissionStatement}</h1>
            <p>
                Auf unserem schönen Hof im lippischen Entrup bei Lemgo werden von der Familie Hanken seit Generationen Getreide, Futterrüben und andere Feldfrüchte angebaut. Vor über 30 Jahren legte August Hanken das erste Erdbeerfeld an. In der Feldmark konnte selbst gepflückt werden, und an die Hauptstrasse wurde ein Verkaufswagen gestellt, wie auch heute noch einer dort steht.
            </p>
            <p>
                Seither hat es einen Generationswechsel, Selbstpflückfelder auf Flächen befreundeter Bauern und die Einführung weiterer Produkte für die Direktvermarktung gegeben – aber eins ist immer gleich geblieben: wir sind Vollblut-Landwirte und erzeugen mit Sachverstand, Sorgfalt, Hingabe und im Einklang mit der Natur Lebensmittel für die Menschen, die hier leben. 100% regional, natürlich, lecker.
            </p>
        </div>
        <div class="immergruen-full-width">
            „Sieh, das Gute liegt so nah“ – Johann Wolfgang von Goethe
        </div>
        <div>
            <h2>Unsere Produkte</h2>            
            <p>
                Wir bauen auf unserem landwirtschaftlichen Familienbetrieb Weizen, Roggen, Gerste, Raps, Zuckerrüben und andere für die Region typische Ackerfrüchte an, die wir an den lokalen Landhandel, nahgelegene Mühlen und die Zuckerfabrik vermarkten.
            </p>          
            <p>
                Für Sie direkt von uns beziehbar:
            </p>
            <div class="card-deck mt-5">
                <div class="card">
                    <img src="../img/static/hanken-entrup-erdbeer-schalen-nah_545.webp" class="card-img-top" alt="Erdbeeren">
                    <div class="card-body d-flex flex-column justify-content-between">
                    <div><h3 class="card-title">Erdbeeren</h3>
                    <p class="card-text">In der Saison reifen auf unseren und von befreundeten Landwirten gepachteten Flächen in ganz Lippe köstliche Erdbeeen, die Sie selbst pflücken oder am Feldrand und in ausgewählten Supermärkten mitnehmen können.</p>
                    </div><a href={ErdbeerenAction}  role="button"  class="btn btn-secondary mwmc">Mehr erfahren</a>
                    </div>
                </div>
                <div class="card">
                    <img src="../img/static/hanken-entrup-kartoffeln-pur_545.webp" class="card-img-top" alt="Kartoffeln">
                    <div class="card-body d-flex flex-column justify-content-between">
                    <div><h3 class="card-title">Kartoffeln</h3>
                    <p class="card-text">Die Kartoffeln von unseren Äckern lagern wir ohne chemische Hilfsstoffe und Begasung mit einer ausgeklügelten Temperatur- und Belüftungsstrategie, damit Sie sie immer im optimalen Zustand im Kartoffelhäuschen vorfinden.</p>
                    </div><a href={KartoffelnAction} role="button"  class="btn btn-secondary mwmc">Mehr erfahren</a>
                    </div>
                </div>
            </div>
            <br>
            <div class="card-deck">
                <div class="card">
                    <img src="../img/static/hanken-entrup-huehnermobil_545.webp" class="card-img-top" alt="Eier">
                    <div class="card-body d-flex flex-column justify-content-between">
                    <div><h3 class="card-title">Eier</h3>
                    <p class="card-text">Unser Hühnermobil wird jede Woche umgesetzt, so daß die Hühner ganzjährig tagsüber draußen und auf frischem Grund herumlaufen können. Die Eier sind im Entruper Kartoffelhäuschen erhältlich.</p>
                    </div><a href={EierAction} role="button" class="btn btn-secondary mwmc">Mehr erfahren</a>
                    </div>
                </div>
                <div class="card">
                    <img src="../img/static/hanken-entrup-kuerbisse_545.webp" class="card-img-top" alt="Kürbisse">
                    <div class="card-body d-flex flex-column justify-content-between">
                    <div><h3 class="card-title">Kürbisse</h3>
                    <p class="card-text">Wenn es sich ergibt, säen wir auf einem hofnahen Feld auch Speise- und Zierkürbisse und Schlangengurken. Der Verkaufsstand wird dann in der Erntezeit in Entrup gegenüber dem Kartoffelhäuschen aufgebaut.</p>
                    </div><a href={KuerbisseAction} role="button" class="btn btn-secondary mwmc">Mehr erfahren</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="jumbotron">
            <h2>{hankenSlogan}</h2>
            <p class="lead">Wir schreiben Ihnen hier auf der Webseite, wenn es etwas Aktuelles zu berichten gibt. Aber eigentlich sprechen wir weniger durch Worte als durch unsere Erdbeeren, Kartoffeln, Kürbisse, Eier. Daher wollen wir es Ihnen natürlich möglichst leicht machen, unsere Erzeugnisse zu finden!</p>
            <div class="row row-cols-1 row-cols-md-2 mb-2">
                <div class="col mb-4 br-4">
                    <div class="card">
                        <h3>{hankenWoErhaeltlich}</h3>
                        <p>Wir haben an vielen Orten in Lippe Erdbeer-Selbstpflückfelder sowie Verkaufsstände für Erdbeeren, Kartoffeln und Eier, und Sie finden unsere Produkte finden auch in mehreren Supermärkten.</p>
                        <a class="btn btn-primary" href={PlacesAction} role="button">Verkaufsstellen</a>
                    </div>
                </div>
                <div class="col mb-4">
                    <div class="card">
                        <h3>{hankenWasAktuellProdukt}</h3>
                        <p>Wenn die Saison beginnt oder endet, wenn wir eine neue Verkaufsstelle eröffnen, wenn sich in unserem Sortiment etwas tut oder es auf unserem Betrieb sonst etwas zu berichten gibt:</p>
                        <a class="btn btn-secondary" href={PostsAction} role="button">Aktuelles</a>
                    </div>
                </div>
            </div>
        </div>

        {renderImgFullWidth "hanken-entrup-hof-panorama"}

        <div>
            <h2>Unser Betrieb</h2>
            <p>
                Wir sind ein Familienbetrieb, und zwar gleich doppelt: zwei Hanken-Brüder und ihre Frauen und Kinder, die auf dem Hof leben und den Boden beackern, der seit Generationen uns und die Familien unserer Mitarbeiter ernährt. Unsere Produkte wiederum ernähren viele Menschen, die sich auf uns verlassen können.</p>
            <p>
                <a href={NachhaltigkeitAction} role="button">Wollen Sie mehr erfahren über unsere Nachhaltigkeit im Umgang mit der Natur</a>
            </p>
            <p>&nbsp;</p>
        </div>

        <div class="image-container">
            <div style="--aspect-ratio:2/1;">
                <picture>
                    <source media={imgMinWidthBig} srcset="../img/static/hanken-entrup-familie-2-1_1110.webp"/>
                    <source media={imgMinWidthMedium} srcset="../img/static/hanken-entrup-familie-2-1_690.webp"/>
                    <img src="../img/static/hanken-entrup-familie-2-1_545.webp" alt="Familie Hanken"/>
                </picture>
            </div>
        </div>

        <p style="font-size:90%">
            Jens Hanken und Tochter Theresa, Erik Hanken
        </p>
        <div class="immergruen-full-width">
            Hanken Entrup – {hankenMissionStatement }
        </div>
        <div>
            <h2>Reiterhof</h2>
            <p>Die Decken in den alten Fachwerkhäusern sind zu niedrig für die modernen Maschinen, die wir brauchen, und seit Menschengedenken hat es auf dem Hof Hanken Pferde gegeben. Daher bieten wir Einstellern Pferdeboxen mit angrenzendem Paddock an. Ein Reitplatz und ein weitläufiges Ausreitgebiet gibt es auch.</p>
            <p><a href={ReiterhofAction} role="button">Wollen Sie ein Pferd einstellen oder reiten?</a></p>
        </div>
        <div class="jumbotron">
            <h2>{hankenSlogan}</h2>
            <p class="lead">Wir schreiben Ihnen hier auf der Webseite, wenn es etwas Aktuelles zu berichten gibt. Aber eigentlich sprechen wir weniger durch Worte als durch unsere Erdbeeren, Kartoffeln, Kürbisse, Eier. Daher wollen wir es Ihnen natürlich möglichst leicht machen, unsere Erzeugnisse zu finden!</p>
            <div class="row row-cols-1 row-cols-md-2 mb-2">
                <div class="col mb-4 br-4">
                    <div class="card">
                        <h3>{hankenWoErhaeltlich}</h3>
                        <p>Wir haben an vielen Orten in Lippe Erdbeer-Selbstpflückfelder sowie Verkaufsstände für Erdbeeren, Kartoffeln und Eier, und Sie finden unsere Produkte finden auch in mehreren Supermärkten!</p>
                        <a class="btn btn-primary" href={PlacesAction} role="button">Verkaufsstellen</a>
                    </div>
                </div>
                <div class="col mb-4">
                    <div class="card">
                        <h3>{hankenWasAktuellProdukt}</h3>
                        <p>Wenn die Saison beginnt oder endet, wenn wir eine neue Verkaufsstelle eröffnen, wenn sich in unserem Sortiment etwas tut oder es auf unserem Betrieb sonst etwas zu berichten gibt:</p>
                        <a class="btn btn-secondary" href={PostsAction} role="button">Aktuelles</a>
                    </div>
                </div>
            </div>
        </div>
|]
