module Web.View.Layout (defaultLayout, Html) where

import IHP.ViewPrelude
import IHP.Environment
import qualified Text.Blaze.Html5            as H
import qualified Text.Blaze.Html5.Attributes as A
import Generated.Types
import IHP.Controller.RequestContext
import Web.Types
import Web.Routes
import Application.Helper.View


defaultLayout :: Html -> Html
defaultLayout inner = H.docTypeHtml ! A.lang "de" $ [hsx|
<head>
    <title>{pageTitleOrDefault hankenTitle}</title>
    {metaThisPage}
    {metaTags}
    {stylesheets}
    {scriptsHead}
</head>
<body>
    <div class={classes ["container", ("welcome", isActivePath WelcomeAction)]}>
        {navbar}
        {renderFlashMessages}
        {inner}
    </div>
    {renderSlogan}
    {renderFooter}
    {scriptsBottom}
</body>
|]


navbar :: Html
navbar = [hsx|
<nav class="navbar navbar-expand-lg navbar-light" style="background-color: white;">
  <a class="navbar-brand" href="/">{renderBrandMenu}</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent" style="padding-top:11px">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
        <a class={classes ["nav-link dropdown-toggle", ("active", isProduktPath)]} href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Produkte
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class={classes ["dropdown-item", ("active", isActivePath ErdbeerenAction)]} href={ErdbeerenAction}>Erdbeeren</a>
          <a class={classes ["dropdown-item", ("active", isActivePath KartoffelnAction)]} href={KartoffelnAction}>Kartoffeln</a>
          <!-- <a class={classes ["dropdown-item", ("active", isActivePath KuerbisseAction)]} href={KuerbisseAction}>Kürbisse</a> -->
          <a class={classes ["dropdown-item", ("active", isActivePath EierAction)]} href={EierAction}>Eier</a>
        </div>
      </li>
      <li class="nav-item">
        <a class={classes ["nav-link", ("active", isActivePath ReiterhofAction)]} href={ReiterhofAction}>Reiterhof</a>
      </li>
      <li class="nav-item">
        <a class={classes ["nav-link", ("active", isActivePath NachhaltigkeitAction)]} href={NachhaltigkeitAction}>Nachhaltigkeit</a>
      </li>
      <li class="nav-item">
        <a class={classes ["nav-link", ("active", isActivePath PostsAction)]} href={PostsAction}>Aktuelles</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <a href={PlacesAction} class="btn btn-secondary my-2 my-sm-0">Verkaufsstellen</a>
    </form>
  </div>
</nav>
|]
    where
        isProduktPath = isActivePath ErdbeerenAction || isActivePath KartoffelnAction || isActivePath KuerbisseAction || isActivePath EierAction


metaThisPage :: Html
metaThisPage = [hsx| 
    {descriptionOrDefault hankenMissionStatement}
    {ogTitleOrDefault hankenTitle}
    {ogTypeOrDefault "article"}
    {ogDescriptionOrDefault hankenMissionStatement}
    {ogUrl}
    {ogImage}
|]