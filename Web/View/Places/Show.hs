module Web.View.Places.Show where
import Web.View.Prelude

data ShowView = ShowView { place :: Place }

instance View ShowView where
    html ShowView { .. } = [hsx|
        <div style="font-size:90%;text-align:right">
            Kartendaten: © 
            <a href="https://openstreetmap.org/copyright">OpenStreetMap</a>-Mitwirkende, 
            SRTM | Kartendarstellung: © 
            <a href="http://opentopomap.org/">OpenTopoMap</a>
            (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)
        </div>
        <div class="image-container">
            <div style="--aspect-ratio:3/1;">
                <picture>
                    <source media={imgMinWidthBig} srcset={imgFilePathBig}/>
                    <source media={imgMinWidthMedium} srcset={imgFilePathMedium}/>
                    <img src={imgFilePathSmall} alt={get #imgFilename place}/>
                </picture>
            </div>
        </div>

        {breadcrumb}

        <a href={renderOpenStreetMapUrl} class="btn btn-secondary" target="_blank">Open Street Map</a>&nbsp;
        <a href={renderGoogleMapsUrl} class="btn btn-primary" target="_blank">Google Maps</a>&nbsp;&nbsp;

        <h1>{get #name place}</h1>
        <div class="post-body">
            {placeTypeCustom (get #placeType place)}<br>
            Saison: {get #saison place}<br>
            {("Öffnungszeiten: " <> get #hours place) |> renderMarkdown}
        </div>
        <hr>
        <div class="post-body">
            {get #description place |> renderMarkdown}
        </div>
    |]
        where
            breadcrumb = renderBreadcrumb
                [ breadcrumbLinkExternal "Hanken Entrup" "/"
                , breadcrumbLink "Verkaufsstellen" PlacesAction
                , breadcrumbText [hsx|{get #name place}|]
                ]

            -- https://developers.google.com/maps/documentation/urls/get-started#forming-the-url
            renderGoogleMapsUrl :: Text
            renderGoogleMapsUrl =
                   "https://www.google.com/maps/search/?api=1&query="
                <> show (x xy)
                <> ","
                <> show (y xy)
                where
                    xy = get #location place

            renderOpenStreetMapUrl :: Text
            renderOpenStreetMapUrl =
                   "https://www.openstreetmap.org/?"
                <> "mlat="
                <> show (x xy)
                <> "&mlon="
                <> show (y xy)
                <> "#map=17/"
                <> show (x xy)
                <> "/"
                <> show (y xy)
                where
                    xy = get #location place

            imgFilePathBig :: Text
            imgFilePathBig = "../img/place/" <> get #imgFilename place <> "_" <> show (filesize imgBig) <> ".webp"

            imgFilePathMedium :: Text
            imgFilePathMedium = "../img/place/" <> get #imgFilename place <> "_" <> show (filesize imgMedium) <> ".webp"

            imgFilePathSmall :: Text
            imgFilePathSmall = "../img/place/" <> get #imgFilename place <> "_" <> show (filesize imgSmall) <> ".webp"
