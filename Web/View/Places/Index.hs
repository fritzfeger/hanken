module Web.View.Places.Index where
import Web.View.Prelude

data IndexView = IndexView { places :: [ Place ]  }

instance View IndexView where
    html IndexView { .. } = [hsx|
        {renderImgFullWidth "hanken-entrup-erdbeer-selbstpflueck"}
        <h1>Verkaufsstellen</h1>
        <p>
            Unsere Kartoffelhäuschen sind an 364 Tagen im Jahr geöffnet. In einigen sind auch Eier erhältlich.
        </p>
        <p>
            Da Erdbeeren nur extrem kurz gelagert werden können, sind die Selbstpflückfelder und Verkaufsstände nur in der Saison für Sie da. In der Tabelle sehen Sie, ob die Saison bei Ihrer nächstgelegenen Verkaufsstelle gerade läuft.
        </p>
        <p>
            <a href={PlacesAction} role="button" 
                class={classes ["btn btn-secondary", ("selected", isActivePath PlacesAction)]}>Alle Produkte</a>
            {forEach (allEnumValues @Product) renderProduct}
        </p>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Ort</th>
                        <th>Saison</th>
                        <th>Öffnungszeiten</th>
                        <th>Produkt</th>
                        <th>Beschreibung</th>
                    </tr>
                </thead>
                <tbody>{forEach places renderPlace}</tbody>
            </table>            
        </div>
    |]
        where
            renderProduct product = case product of
                Nachhaltigkeit -> [hsx||]
                Betrieb        -> [hsx||]
                _              -> [hsx|
                                        <a href={produktPathQuery product} role="button" 
                                            class={classes ["btn btn-secondary", ("selected", isProduktPath)]}>{productCustom product}</a>&nbsp;
                                    |]
                                        where
                                            produktPathQuery product = "/Places?product=" <> show product |> toLower
                                            isProduktPath = isActivePath (produktPathQuery product)


renderPlace :: Place -> Html
renderPlace place = [hsx|
    <tr>
        <td><a href={ShowPlaceAction (get #id place)}>{get #name place}</a></td>
        <td>{saisonStatusBadge (get #saison place)}</td>
        <td>{get #hours place |> renderMarkdown}</td>
        <td>{productCustom (get #product place)}</td>
        <td>{get #description place |> renderMarkdown}</td>
    </tr>
|]
