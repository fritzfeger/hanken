module Web.Routes where
import IHP.RouterPrelude
import Generated.Types
import Web.Types

-- Generator Marker
instance AutoRoute PostController
instance AutoRoute PlacesController

instance HasPath StaticController where
    pathTo WelcomeAction        = "/"
    pathTo DatenschutzAction    = "/datenschutz"
    pathTo ImpressumAction      = "/impressum"
    pathTo ErdbeerenAction      = "/produkte/erdbeeren"
    pathTo KartoffelnAction     = "/produkte/kartoffeln"
    pathTo EierAction           = "/produkte/eier"
    pathTo KuerbisseAction      = "/produkte/kuerbisse"
    pathTo ReiterhofAction      = "/reiterhof"
    pathTo NachhaltigkeitAction = "/nachhaltigkeit"

instance CanRoute StaticController where
    parseRoute' =
        (string "/" <* endOfInput >> pure WelcomeAction )
        <|> (string "/datenschutz" <* endOfInput >> pure DatenschutzAction)
        <|> (string "/impressum" <* endOfInput >> pure ImpressumAction)
        <|> (string "/produkte/erdbeeren" <* endOfInput >> pure ErdbeerenAction)
        <|> (string "/produkte/kartoffeln" <* endOfInput >> pure KartoffelnAction)
        <|> (string "/produkte/eier" <* endOfInput >> pure EierAction)
        <|> (string "/produkte/kuerbisse" <* endOfInput >> pure KuerbisseAction)
        <|> (string "/reiterhof" <* endOfInput >> pure ReiterhofAction)
        <|> (string "/nachhaltigkeit" <* endOfInput >> pure NachhaltigkeitAction)