module Web.Types where

import IHP.Prelude
import IHP.ModelSupport
import Generated.Types

data WebApplication = WebApplication deriving (Eq, Show)


data StaticController 
    = WelcomeAction
    | DatenschutzAction
    | ImpressumAction
    | ErdbeerenAction
    | KartoffelnAction
    | EierAction
    | KuerbisseAction
    | ReiterhofAction
    | NachhaltigkeitAction
    deriving (Eq, Show, Data)

data PostController
    = PostsAction
    | ShowPostAction { postId :: !(Id Post) }
    deriving (Eq, Show, Data)

data PlacesController
    = PlacesAction
    | ShowPlaceAction { placeId :: !(Id Place) }
    deriving (Eq, Show, Data)
