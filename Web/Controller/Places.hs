module Web.Controller.Places where

import Web.Controller.Prelude
import Web.View.Places.Index
import Web.View.Places.Show
import Application.Helper.View

instance Controller PlacesController where
    action PlacesAction = do
        let placesProduct = paramOrNothing @Product "product"
        (postQ, pagination) <- query @Place
            |> orderBy #saison -- Enums in PostgreSQL nach ERSTELLUNGSDATUM sortiert, buh! https://stackoverflow.com/questions/8840228/postgresql-using-a-calculated-column-in-the-same-query/36530228
            |> orderBy #name
            |> paginate
        places <- case placesProduct of
            Just placesProduct -> postQ
                |> filterWhere (#product, placesProduct)
                |> fetch
            Nothing -> postQ
                |> fetch
        render IndexView { .. }

    action ShowPlaceAction { placeId } = do
        place <- fetch placeId

        setTitle         (get #name place)
        setOGTitle       (get #name place)
        setDescription   (get #description place)
        setOGDescription (get #description place)
        setOGImage       hankenLogoUrl
        setOGUrl         (urlTo ShowPlaceAction { .. })

        -- case get #imageUrl post of
        --     Just url -> setOGImage url  -- nur, falls es je Images pro Beitrag geben sollte
        --     Nothing -> pure ()          -- When setOGImage is not called, the og:image tag will not be rendered

        render ShowView { .. }
