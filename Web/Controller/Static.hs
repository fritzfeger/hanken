module Web.Controller.Static where
import Web.Controller.Prelude
import Web.View.Static.Welcome
import Web.View.Static.Datenschutz
import Web.View.Static.Erdbeeren
import Web.View.Static.Kartoffeln
import Web.View.Static.Eier
import Web.View.Static.Kuerbisse
import Web.View.Static.Impressum
import Web.View.Static.Reiterhof
import Web.View.Static.Nachhaltigkeit

instance Controller StaticController where
    action WelcomeAction = render WelcomeView
    action ImpressumAction = render ImpressumView
    action DatenschutzAction = render DatenschutzView
    action ErdbeerenAction = render ErdbeerenView
    action KartoffelnAction = render KartoffelnView
    action EierAction = render EierView
    action KuerbisseAction = render KuerbisseView
    action ReiterhofAction = render ReiterhofView
    action NachhaltigkeitAction = render NachhaltigkeitView
