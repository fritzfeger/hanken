module Web.Controller.Post where

import Web.Controller.Prelude
import Web.View.Post.Index
import Web.View.Post.Show
import qualified Text.MMark as MMark
import Application.Helper.View


instance Controller PostController where
    action PostsAction = do
        let postsProduct = paramOrNothing @Product "product"
        (postQ, pagination) <- query @Post
            |> orderByDesc #createdAt
            |> paginate
        post <- case postsProduct of
            Just postsProduct -> postQ
                |> filterWhere (#product, postsProduct)
                |> fetch
            Nothing -> postQ
                |> fetch
        render IndexView { .. }

    action ShowPostAction { postId } = do
        post <- fetch postId

        post |> incrementField #visitCount |> updateRecord
        setTitle         (get #title post)
        setOGTitle       (get #title post)
        setDescription   (get #excerpt post)
        setOGDescription (get #excerpt post)
        setOGImage       hankenLogoUrl
        setOGUrl         (urlTo ShowPostAction { .. })

        -- case get #imageUrl post of
        --     Just url -> setOGImage url  -- nur, falls es je Images pro Beitrag geben sollte
        --     Nothing -> pure ()          -- When setOGImage is not called, the og:image tag will not be rendered

        render ShowView { .. }