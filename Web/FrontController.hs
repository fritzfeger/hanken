module Web.FrontController where

import IHP.RouterPrelude
import Web.Controller.Prelude
import Web.View.Layout (defaultLayout)

-- Controller Imports
import Web.Controller.Places
import Web.Controller.Post
import Web.Controller.Static
import Admin.Controller.Prelude

instance FrontController WebApplication where
    controllers = 
        [ startPage Web.Controller.Prelude.WelcomeAction
        -- Generator Marker
        , parseRoute @Web.Controller.Prelude.PlacesController
        , parseRoute @Web.Controller.Prelude.PostController
        , parseRoute @Web.Controller.Prelude.StaticController
        ]

instance InitControllerContext WebApplication where
    initContext = do
        setLayout defaultLayout
        initAutoRefresh
