# Shipnix recommended settings
# IMPORTANT: These settings are here for ship-nix to function properly on your server
# Modify with care

{ config, pkgs, modulesPath, lib, ... }:
{
  nix = {
    package = pkgs.nixUnstable;
    extraOptions = ''
      experimental-features = nix-command flakes ca-derivations
    '';
    settings = {
      trusted-users = [ "root" "ship" "nix-ssh" ];
    };
  };

  programs.git.enable = true;
  programs.git.config = {
    advice.detachedHead = false;
  };

  services.openssh = {
    enable = true;
    # ship-nix uses SSH keys to gain access to the server
    # Manage permitted public keys in the `authorized_keys` file
    passwordAuthentication = false;
    #  permitRootLogin = "no";
  };


  users.users.ship = {
    isNormalUser = true;
    extraGroups = [ "wheel" "nginx" ];
    # If you don't want public keys to live in the repo, you can remove the line below
    # ~/.ssh will be used instead and will not be checked into version control. 
    # Note that this requires you to manage SSH keys manually via SSH,
    # and your will need to manage authorized keys for root and ship user separately
    openssh.authorizedKeys.keyFiles = [ ./authorized_keys ];
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCwqJI58wzS6WGv4kHGh+8QQp8QeU4UrdmRa4/wUgI7lDxhC4jt/LYbonWug2tWmQHNk3W8I05UB7o6zmDkmGz35yLenZ7cfB0CPbVdoGRWZ6iha7Yt0VtumQE4A5+DYfXfXI36x65D2MxcS8sESFodCuEMBwRAKk+BluvYRlfI50+tMc7TYkGTtjP9HE/6ApugomALcTFry9ekCTOXKceI/XV9ddP+MMGxzwPYeUWpr/FEfG3DQntpQX+IZQIAPnvX6IVY6rl17jSgzqFxhH768uNdDpjXKGd8yTMr23yDf351+uZdcYeiRHpHEfTMDYQyg9OqhxldZ+I44KtuT8AFbJpCCb2C1CaKmMaY5LyEuGmlcPf0zicQTDTXNfJs7YQUxZbVPnGpacDvbzJ2qUsEv9rAj9y0IzOn+YM+PubhdAoAVyhMlrWRkD4lg7tK4cjjdxqkDqS7Zegde2bPCmRbXZ/HbIBtmH0wIm58FnAFUeaOscvdsDxwtaXRr+duWNk= ship@tite-ship
"
    ];
  };

  # Can be removed if you want authorized keys to only live on server, not in repository
  # Se note above for users.users.ship.openssh.authorizedKeys.keyFiles
  users.users.root.openssh.authorizedKeys.keyFiles = [ ./authorized_keys ];
  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCwqJI58wzS6WGv4kHGh+8QQp8QeU4UrdmRa4/wUgI7lDxhC4jt/LYbonWug2tWmQHNk3W8I05UB7o6zmDkmGz35yLenZ7cfB0CPbVdoGRWZ6iha7Yt0VtumQE4A5+DYfXfXI36x65D2MxcS8sESFodCuEMBwRAKk+BluvYRlfI50+tMc7TYkGTtjP9HE/6ApugomALcTFry9ekCTOXKceI/XV9ddP+MMGxzwPYeUWpr/FEfG3DQntpQX+IZQIAPnvX6IVY6rl17jSgzqFxhH768uNdDpjXKGd8yTMr23yDf351+uZdcYeiRHpHEfTMDYQyg9OqhxldZ+I44KtuT8AFbJpCCb2C1CaKmMaY5LyEuGmlcPf0zicQTDTXNfJs7YQUxZbVPnGpacDvbzJ2qUsEv9rAj9y0IzOn+YM+PubhdAoAVyhMlrWRkD4lg7tK4cjjdxqkDqS7Zegde2bPCmRbXZ/HbIBtmH0wIm58FnAFUeaOscvdsDxwtaXRr+duWNk= ship@tite-ship
"
  ];

  security.sudo.extraRules = [
    {
      users = [ "ship" ];
      commands = [
        {
          command = "ALL";
          options = [ "NOPASSWD" "SETENV" ];
        }
      ];
    }
  ];
}
